package fi.vamk.e1700811;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlayerController {
	@Autowired
	private PlayerRepository repository;

	@GetMapping("/players")
	public Iterable<Player> list() {
		return repository.findAll();
	}

	@GetMapping("/player/{name}")
	public Player get(@PathVariable("name") String name) {
		return repository.findByName(name);
	}

	@PostMapping("/player")
	public @ResponseBody Player create(@RequestBody Player player) throws IOException {
		return repository.save(player);		
	}

	@PutMapping("/player")
	public @ResponseBody Player update(@RequestBody Player player) {
		return repository.save(player);
	}

	@DeleteMapping("/player")
	public void delete(@RequestBody Player player) {
		repository.delete(player);
	}
}