package fi.vamk.e1700811;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ScoreboardApplication {
	
	@Autowired
	private PlayerRepository playerRepository;
	@Autowired
	private ScoreRepository scoreRepository;
	

	public static void main(String[] args) {
		SpringApplication.run(ScoreboardApplication.class, args);
	}
	
	@Bean
	public void initData() {
		List<Score> scores = new ArrayList<>();
		
		Player player = new Player("Jari Litmanen", "Finland", "Ajax");
		Player player2 = new Player("Lionel Messi", "Argentina", "Barcelona");
		Player player3 = new Player("CR7", "Portugal", "Juventus");
		player = playerRepository.save(player);
		player2 = playerRepository.save(player2);
		player3 = playerRepository.save(player3);
		
		scores.add(new Score(player, 2012, 12));
		scores.add(new Score(player, 2013, 35));
		scores.add(new Score(player, 2014, 50));
		scores.add(new Score(player, 2015, 22));
		scores.add(new Score(player2, 2012, 70));
		scores.add(new Score(player2, 2013, 10));
		scores.add(new Score(player2, 2014, 95));
		scores.add(new Score(player2, 2015, 40));
		scores.add(new Score(player3, 2012, 32));
		scores.add(new Score(player3, 2013, 67));
		scores.add(new Score(player3, 2014, 86));
		scores.add(new Score(player3, 2015, 102));
		
		for(Score score : scores) {
			scoreRepository.save(score);
		}
	}
}
