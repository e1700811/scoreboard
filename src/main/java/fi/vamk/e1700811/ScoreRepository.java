package fi.vamk.e1700811;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ScoreRepository extends CrudRepository<Score, Integer>{

	public List<Score> findAllBySeason(int season);
	public List<Score> findAllByPlayer(Player player);
	
}