package fi.vamk.e1700811;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;


@Entity
@NamedQuery(name="Player.findAll", query="Select p FROM Player p")
public class Player implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int playerId;
    private String name;
    private String nationality;
    private String team;
    
    public Player(){}
    
    public Player(String name){
        this.name = name;
    }

    public Player(String name, String nationality) {
        this.name = name;
        this.nationality = nationality;
    }

    public Player(String name, String nationality, String team) {
        this.name = name;
        this.nationality = nationality;
        this.team = team;
    }
    
    public Player(int id, String name, String nationality, String team){
        this.playerId = id;
        this.name = name;
        this.nationality = nationality;
        this.team = team;
    }
    
    public int getId(){
        return this.playerId;
    }
    
    public void setId(int id){
        this.playerId = id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getNationality() {
    	return this.nationality;
    }

    public void setNationality(String nationality){
        this.nationality = nationality;
    }
    
    public String getTeam() {
    	return this.team;
    }

    public void setTeam(String team){
        this.team = team;
    }
    
    public String toString() {
    	return playerId + " " + name + " " + nationality + " " + team;
    }
    
}