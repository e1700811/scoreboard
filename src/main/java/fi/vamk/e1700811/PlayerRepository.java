package fi.vamk.e1700811;

import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, Integer>{

	public Player findByName(String name);
	
}