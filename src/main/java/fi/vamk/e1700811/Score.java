package fi.vamk.e1700811;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;


@Entity
@NamedQuery(name="Score.findAll", query="Select s FROM Score s")
public class Score implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int scoreId;
	
	@ManyToOne
	@JoinColumn(name="playerId")
    private Player player;
    private int season;
    private int goals;
    
    public Score(){}
    
    public Score(int season, int goals) {
        this.season = season;
        this.goals = goals;
    }

    public Score(Player player, int season, int goals) {
        this.player = player;
        this.season = season;
        this.goals = goals;
    }
    
    public Score(int id, Player player, int season, int goals) {
        this.scoreId = id;
    	this.player = player;
        this.season = season;
        this.goals = goals;
    }

	public int getId(){
        return this.scoreId;
    }
    
    public void setId(int id){
        this.scoreId = id;
    }
    
    public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getGoals() {
		return goals;
	}

	public void setGoals(int goals) {
		this.goals = goals;
	}

     public String toString() {
    	return scoreId + " " + player.getName() + " " + season + " " + goals;
    }
    
}