package fi.vamk.e1700811;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScoreController {
	@Autowired
	private ScoreRepository repository;
	@Autowired
	private PlayerRepository playerRepo;

	@GetMapping("/scores")
	public Iterable<Score> list() {
		return repository.findAll();
	}

	@GetMapping("/scoresByPlayer/{name}")
	public Iterable<Score> get(@PathVariable("name") String name) {
		Player player = playerRepo.findByName(name);
		return repository.findAllByPlayer(player);
	}
	
	@GetMapping("/scoresByYear/{year}")
	public Iterable<Score> get(@PathVariable("year") int year) {
		return repository.findAllBySeason(year);
	}

	@PostMapping("/score")
	public @ResponseBody Score create(@RequestBody Score score) throws IOException {
		return repository.save(score);		
	}

	@PutMapping("/score")
	public @ResponseBody Score update(@RequestBody Score score) {
		return repository.save(score);
	}

	@DeleteMapping("/score")
	public void delete(@RequestBody Score score) {
		repository.delete(score);
	}
}