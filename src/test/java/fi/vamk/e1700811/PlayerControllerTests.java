package fi.vamk.e1700811;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import fi.vamk.e1700811.Player;
import fi.vamk.e1700811.PlayerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1700811" })
@EnableJpaRepositories(basePackageClasses = PlayerRepository.class)
public class PlayerControllerTests {
	
	@Autowired
	private PlayerRepository repository;
	
	private String testName = "Jorma Uotinen";
	private String testNationality = "Finland";
	private String testTeam = "VPS";
	
	@BeforeEach
	public void postPlayer() {
        Player player = new Player(testName, testNationality, testTeam);
        Player savedPlayer = repository.save(player);
        assertNotEquals(savedPlayer.getId(), 0);
	}
	
	@Test
	public void getPlayerByName() {
        Player foundByName = repository.findByName(testName);
        assertEquals(foundByName.getName(), testName);	
	}

	@Test
	public void deletePlayer() {
		Iterable<Player> begin = repository.findAll();
		Player foundByName = repository.findByName(testName);
        repository.delete(foundByName);
        Iterable<Player> end = repository.findAll();
        assertNotEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
	}

}