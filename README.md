# Scoreboard API
Restful api project created with Spring Java which saves player information and scores related to players.
Api baseurl http://e1700811scoreboard.herokuapp.com/

## Installation
1. Get credentials for authorization to use api
2. Make basic api call from your client to any of the endpoints with credentials in request header

## Endpoints
1. /scores - Get all scores by all players in database
2. /scoresByPlayer/{playerName} - Get all scores by one player
3. /scoresByYear/{year} - Get all scores by all player in specified year

## Contribution
Project is free for anyone to modify, copy or break anytime they want